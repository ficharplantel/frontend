import { RouterModule, Routes } from '@angular/router';


import { LoginComponent} from './components/login/login.component';
import {JugadoresclubComponent} from './components/jugadoresclub/jugadoresclub.component';
import {JugadoreslibresComponent} from './components/jugadoreslibres/jugadoreslibres.component';
import { DaraltaComponent } from './components/daralta/daralta.component';

const routes: Routes = [

    { path: 'login', component: LoginComponent },
    { path: 'daralta', component: DaraltaComponent },
    { path: 'jugadoresclub/:id', component: JugadoresclubComponent },
    { path: 'jugadoresdis', component: JugadoreslibresComponent },
    { path: '**', pathMatch:'full', redirectTo: 'login' }


]

export const appRouting = RouterModule.forRoot(routes);