import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';
import { NgForm } from '@angular/forms';
import {AppService} from '../service/app.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  isLogged = false;

  constructor( private router:Router, private appservice: AppService ) { }

   loginClub = {
    nombre: '',
    password: '' 
   }

  ngOnInit() {
    this.appservice.getUsuarioAPI().subscribe(data => {
      console.log(data)
      this.appservice.setDatosUsuarios(data.results);
    },
       error => {
         console.log("fallo el call de la API");
       
         console.log(error)
       });
  }

  datos(forma:NgForm) {
  console.log(forma);
  console.log(forma.value);
  this.isLogged = this.appservice.check(this.loginClub);
    if(this.isLogged){
      if(this.appservice.getDatosUsuario().name=="AFA"){
        this.appservice.setIdAfa(this.appservice.getDatosUsuario().id)
        console.log("Id AFA asignada"+this.appservice.getIdAfa());
        
        this.router.navigate(['/daralta']);
      }else{
        this.appservice.cargarIdAFA();
        this.router.navigate(['/jugadoresclub/'+this.appservice.getDatosUsuario().id]);
      }
    }else{
      console.log("No existe Usuario");
      this.router.navigate(['/login']);
    }



}



}
