 import {Http, Headers} from '@angular/http';
import { Injectable } from '@angular/core';
import 'rxjs/Rx';

@Injectable()
export class AppService {
constructor(private http:Http) { }


private jugId: number;
private jugadoresDisponibles =[];
private idAfa: any;
//private iduser: any;
private user ={
  name: '',
  pass: '',
  id: '',
  logo: ''
}
private usuarios = [];

getIdAfa(){

  return this.idAfa
}


check(login:any){
  this.buscarEnUsuariosPorNombre(login.nombre,this.usuarios);
  if(this.user != undefined && this.user.pass == login.password){
    return true;
  }else
   return false;
}

buscarEnUsuariosPorNombre(nom:string,users:any){
  let encontrado = false;
  for(let us of users){
    if(us.nombre == nom){
      encontrado = true;
      this.user.name = us.nombre;
      this.user.logo = us.logo;
      this.user.pass = us.pass;
      this.user.id = us.id;
      break;
    }
  }
  if(!encontrado){
    this.user = undefined;
  }
}

getJugadoresDisponibles(){
    this.jugadoresDisponibles
}


       getJugadoresDisponiblesAPI(id: number) {
        console.log("llama a getJugadoresDisponibles API");
    
        let header = new Headers({ 'Content-Type': 'application/json' });
        let jugadoresdisponiblesURL = "http://192.168.101.100:8080/clubes/clubes/" + id;
    
        return this.http.get(jugadoresdisponiblesURL, { headers: header })
          .map(res => {
            console.log(res.json());
            console.log("entro positivo REST getJugadoresDisponibles")
            this.jugadoresDisponibles = res.json().results;
            return res.json();
          }, err => console.log("error: " + err.json()));
      }
    




       getDatosUsuarios(){

        return this.usuarios;
       }
       getDatosUsuario(){
        return this.user;
      }

       setIdAfa(id:any){
         console.log(id);
        this.idAfa = id;
        console.log(this.idAfa);
       }
  

       setDatosUsuarios(results:any){
         this.usuarios = results;

       }
      
       cargarIdAFA(){
        for(let us of this.usuarios){
          if(us.nombre == "AFA")
          this.idAfa = us.id;
        }
       }
       getUsuarioAPI(){
        console.log("llama a getDatosUsuario API");
    
        let header = new Headers({'Content-Type':'application/json'});
        let usuariosURL = "http://192.168.101.100:8080/clubes/login";
     
         return this.http.get(usuariosURL, {headers: header})
            .map(res =>{
              console.log(res.json());
              console.log("entro positivo REST getClubesAPI")
              this.usuarios = res.json().results;
              return res.json();
            }, err => console.log("error: "+err.json()));
        }



        postJugadoresAPI(form:any){

          console.log("llama a postJugadores API");
        
           let header = new Headers({'Content-Type':'application/json'});
           let crearjugadorURL = "http://192.168.101.100:8080/clubes/jugador";
           let body = form;
           
            return this.http.post(crearjugadorURL, body, {headers: header})
               .map(res =>{
                 console.log(res.json());
                 console.log("entro positivo REST postJugadores API")
                
                 return res.json();
               }, err => console.log("error: "+err.json()));
           }
          //  guardoJugador(id: number) {
          //   this.jugId = id;
          // }
          // aca mando el id
          getidUsuario() {
            return this.jugId;
          }
        
          //llamar al POST de fichaje de la API
          postFicharAPI(id: number){
        
            console.log("llama a postFichar API");
          
             let header = new Headers({'Content-Type':'application/json'});
             let CommentURL = "http://192.168.101.100:8080/clubes/fichaje";
             
             let body = {
               idClub : this.user.id,
               idJug :  id
             }
             console.log(body);
              return this.http.post(CommentURL, body, {headers: header})
                 .map(res =>{
                   console.log(res.json());
                   console.log("entro positivo REST postFicharAPI")
                   return res.json();
                 }, err => console.log("error: "+err.json()));
          }
  



       }



 