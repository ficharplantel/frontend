import { Component, OnInit } from '@angular/core';
import {AppService} from '../service/app.service';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
@Component({
  selector: 'app-daralta',
  templateUrl: './daralta.component.html',
  styleUrls: ['./daralta.component.css']
})
export class DaraltaComponent implements OnInit {

  crearJugador={
    idClub : this.appService.getDatosUsuario().id,
    jugador: {
      nombre : '',
      edad : '',
      foto : ''
    }
  }

  constructor(private appService:AppService, private router:Router) { }

  ngOnInit() {
  }


  creajugador(forma:NgForm) {
    console.log("guardo los cambios");
    console.log("NgForm forma: "+ forma);
    console.log("forma value: "+ forma.value);
    console.log("creoJugador "+ this.crearJugador);
    console.log(this.crearJugador.jugador.nombre) 
    console.log(this.crearJugador.idClub) 
  
    this.appService.postJugadoresAPI(this.crearJugador).subscribe(data => {
      console.log(data)
      
    },
       error => {
         console.log("fallo el call de la API");
       
         console.log(error)
       });
       this.router.navigate(['/jugadoresdis']);
  }
}
