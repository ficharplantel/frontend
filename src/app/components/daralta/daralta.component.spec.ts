import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { DaraltaComponent } from './daralta.component';

describe('DaraltaComponent', () => {
  let component: DaraltaComponent;
  let fixture: ComponentFixture<DaraltaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ DaraltaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DaraltaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
