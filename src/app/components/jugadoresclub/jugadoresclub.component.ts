import { Component, OnInit } from '@angular/core';

import{AppService}from '../service/app.service';

import { ActivatedRoute } from "@angular/router";
import { Router } from '@angular/router';

@Component({
  selector: 'app-jugadoresclub',
  templateUrl: './jugadoresclub.component.html',
  styleUrls: ['./jugadoresclub.component.css']
})
export class JugadoresclubComponent implements OnInit {

  jugadoresclub:any;

  constructor(private appService:AppService, private activatedRouted: ActivatedRoute, private router:Router ) {

    this.activatedRouted.params.subscribe(params =>{
      console.log('Parametro recibido: '+ params['id']);

      this.appService.getJugadoresDisponiblesAPI(params['id'])
  .subscribe(data => {
    console.log("imprimo vuelta de getJugClubAPI");    
    console.log(data)
    console.log(data.results);
    
    this.jugadoresclub = data.results;  //este results es el objeto del json    
    
  },
    error => {
      console.log("fallo el llamado de la API");   
      console.log(error)
    });

    console.log(this.jugadoresclub);    
  })

   }

  ngOnInit() {
  }

}
