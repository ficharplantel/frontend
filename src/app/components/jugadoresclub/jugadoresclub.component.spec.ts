import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JugadoresclubComponent } from './jugadoresclub.component';

describe('JugadoresclubComponent', () => {
  let component: JugadoresclubComponent;
  let fixture: ComponentFixture<JugadoresclubComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JugadoresclubComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JugadoresclubComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
