import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { JugadoreslibresComponent } from './jugadoreslibres.component';

describe('JugadoreslibresComponent', () => {
  let component: JugadoreslibresComponent;
  let fixture: ComponentFixture<JugadoreslibresComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ JugadoreslibresComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(JugadoreslibresComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
