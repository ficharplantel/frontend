import { Component, OnInit } from '@angular/core';
import{AppService}from '../service/app.service';

import { ActivatedRoute } from "@angular/router";
import { Router } from '@angular/router';

@Component({
  selector: 'app-jugadoreslibres',
  templateUrl: './jugadoreslibres.component.html',
  styleUrls: ['./jugadoreslibres.component.css']
})
export class JugadoreslibresComponent implements OnInit {

  jugadoreslib:any;

  constructor(private appService:AppService, private activatedRouted: ActivatedRoute, private router:Router) { 

    this.appService.getJugadoresDisponiblesAPI(this.appService.getIdAfa())
  .subscribe(data => {
    console.log("imprimo vuelta de getJugDisAPI");
    
    console.log(data)
    console.log(data.results);
    
    this.jugadoreslib = data.results;  //este results es el objeto del json
    
    
  },
    error => {
      console.log("fallo el llamado de la API");
   
      console.log(error)
    });

  }

  ngOnInit() {
         
  }

  fichar(id: number){
    console.log("imprimo parametro .ts: ");
    //para pasar a otra pantalla importe el router, dps llamarlo en el constructor
    
    this.appService.postFicharAPI(id).subscribe(data => {
      console.log(data)
      
    },
       error => {
         console.log("fallo el call de la API");
       
         console.log(error)
       }); //guardo el id
    this.router.navigate(['/jugadoresclub/'+this.appService.getDatosUsuario().id]);
    
  }

  
}