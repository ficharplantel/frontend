import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {appRouting} from './app.routes';
import {HttpModule} from '@angular/http';
import { FormsModule} from '@angular/forms';


import { AppComponent } from './app.component';
import { LoginComponent } from './components/login/login.component';
import { NavbarComponent } from './components/shared/navbar/navbar.component';
import { JugadoresclubComponent } from './components/jugadoresclub/jugadoresclub.component';
import { JugadoreslibresComponent } from './components/jugadoreslibres/jugadoreslibres.component';
import { DaraltaComponent } from './components/daralta/daralta.component';
import { AppService } from './components/service/app.service';


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    NavbarComponent,
    JugadoresclubComponent,
    JugadoreslibresComponent,
    DaraltaComponent
  ],
  imports: [
    BrowserModule,
    appRouting,
    HttpModule,
    FormsModule
  ],
  providers: [AppService],
  bootstrap: [AppComponent]
})
export class AppModule { }
